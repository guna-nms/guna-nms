# Guna NMS (Ubuntu Apache LibreNMS)

# Installation and configuration walk through 
 
**Created by:** Rex Ng (Guna NMS)

**Last update:** 1/8/19 (based on ubuntu 19.04)

`Alert` All request and reports should be directed to the [GitLab page](https://gitlab.com/guna-hk/guna-nms), bitbucket repo is only for hosting the iso file

**Purpose:**

After following this guide you shall have a functional NMS with **2 min polling** and **auto discovery** enabled **.**

This guide was created to help GUNA employess ease their future installation, but also helping others who find it difficult to deploy LibreNMS.

**Warning:**

`I am not responsible for any damage of any kind that this guide may cause.`

`Any non guide related problems should be reported to related projects own repo.`



**To access this guide, users can acess the [wiki page](https://gitlab.com/guna-hk/guna-nms/wikis/home)**


Out of the box:
```
guna@guna-nms 
------------- 
OS: Ubuntu 19.04 x86_64           
Kernel: 5.0.0-23-generic                  
Shell: bash 5.0.3                 
Resolution:              
DE: LXQt                          
WM: Openbox
```